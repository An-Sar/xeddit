window.Popper = require('popper.js').default;
window.$ = window.jQuery = require('jquery');
require('bootstrap');

window.axios = require('axios');
window.Vue = require('vue');
window.moment = require('moment');
window.localForage = require('localforage');

localForage.config({
    driver: [localForage.INDEXEDDB, localForage.WEBSQL, localForage.LOCALSTORAGE],
    name: 'xeddit',
    storeName: 'xedditstorage',
    version: 3
});

Vue.component('reddit-index', require('./components/RedditIndexComponent'));

const app = new Vue({
    el: '#app'
});
